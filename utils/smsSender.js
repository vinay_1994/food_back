const AWS = require('aws-sdk');
const SNS = require('./../config/production')

module.exports = (user) => {

    AWS.config.update({ accessKeyId: SNS.SNS.ACCESS_KEY, secretAccessKey: SNS.SNS.SECREATE_KEY })
    const sns = new AWS.SNS({ region: 'us-east-1' });
    

    const params = {
        Message: user.otp + " "+" Is Your OTP for Registration fro food service " ,
        MessageStructure: 'string',
        PhoneNumber: '+91'+user.contactNum
    };
    sns.publish(params, (err, data) => {
       if(err){
           console.log(err);
           throw new Error(err);
       }else{
           return true
       }
    });
}

