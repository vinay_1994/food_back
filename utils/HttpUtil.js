'use strict';

/**
 * @file HttpUtil is util function, It will provide the support functions to create respones objects.
 * @copyright www.neviton.com
 */

/**
 * If request is sucess
 *
 * @param {any} payLoad It is any value to send as response.
 * @param {string} errorMessage It is string value to send as respone message
 * @return {object} {status=200rMessage='OK', payLoad=data }
 */
exports.getSuccess = (msg,data) => {
  return {
    status: 200,
    message : msg,
    payLoad : [data]
  };
};

/**
 * If resource is created.
 *
 * @param {any} payLoad It is any value to send as response.
 * @param {string} errorMessage It is string value to send as respone message
 * @return {object} {status=201rMessage='Created', payLoad=data }
 */
exports.getCreated = (msg = 'Created') => {
  return {
    status: 201,
    message:msg
  };
};

/**
 * If any invalid request or request data.
 *
 * @param {array} msg,is an array of msg,e and msg,sage.
 * @return {object} {status=400errorMessage='Bad Request', payLoad=null }
 */
exports.getBadRequest = (msg,data) => {
  return {
    status: 400,
    message: msg,
    payLoad: [data]
  };
};

/**
 * If any server side Exception.
 *
 * @param {array} msg,is an array of msg,e and msg,sage.
 * @return {object} {status=500errorMessage='Internal Server Error', payLoad=null }
 */
exports.getException = (msg,data) => {
  return {
    status: 500,
    message: msg,
    payLoad: [data]
  };
};

/**
 * If any Unauthorized request.
 *
 * @param {array} msg,is an array of msg,e and msg,sage.
 * @return {object} {status=401errorMessage='Unauthorized', payLoad=null }
 */
// UNAUTHORIZED(401, "Unauthorized"),
exports.getUnauthorized = (msg) => {
  return {
    status: 401,
    message: msg,
    payLoad: null
  };
};

/**
 * If Access denined.
 *
 * @param {array} msg,is an array of msg,e and msg,sage.
 * @return {object} {status=403errorMessage='Forbidden', payLoad=null }
 */
exports.getAccessDenined = (msg='Forbidden') => {
  return {
    status: 403,
    message: msg,
    payLoad: null
  };
};

/**
 * If Requested data or record is not found.
 *
 * @param {array} msg,is an array of msg,e and msg,sage.
 * @return {object} {status=404errorMessage='Not Found', payLoad=null }
 */
exports.getNotFound = (message,payLoad) => {
  return {
    message,
    payLoad
  };
};



