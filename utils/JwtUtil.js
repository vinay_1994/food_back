'use strict'
const JWT = require('jsonwebtoken');
const key = "dont_ask_anything_go";


exports.signJWT = userObj => JWT.sign({
    userId : userObj.catererId,
    userName: userObj.userName,
    area: userObj.area
},key,{
    expiresIn:'1m'
})

exports.verifyJWT = async token => JWT.verify(token,key);