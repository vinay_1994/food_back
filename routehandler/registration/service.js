'use strict';

const SQl = require('./sql');
const dbCOn = require('./../../utils/dbConnection');
const otpGenerator = require('otp-generator')
const smsSender = require('./../../utils/smsSender')

exports.auth={
    userexisting :async reqBody => {
       return await dbCOn.query(SQl.checkExistingUser,reqBody.contactNum);
    },
    saveUser:async reqBody => {
        reqBody['otp'] = otpGenerator.generate(4,{
            specialChars:false,
            alphabets:false,
            upperCase:false
        })
        await dbCOn.query(SQl.saveUser,reqBody);
        await smsSender(reqBody);
        return true;
    },
    login: async reqBody => {
        return await dbCOn.query(SQl.checkExistingUser,reqBody.contactNum)
    },
    checkOtp: async otpObj =>{
        let data = await dbCOn.query(SQl.checkExistingUser,otpObj.contactNum);
        if(data.length === 1 && otpObj.otp == data[0].otp){
            await dbCOn.query(SQl.changeIsconfirmed,[1,otpObj.contactNum])
            return true
        }else{
            throw new Error('Incorrect Otp Entered')
        }
    }
}
