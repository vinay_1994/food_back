module.exports = {
    checkExistingUser : 'select * from tbl_caterer where contactNum = ?',
    saveUser : 'insert into tbl_caterer set ?',
    changeIsconfirmed: 'update tbl_caterer set isConfirmed = ? where contactNum = ?'
}