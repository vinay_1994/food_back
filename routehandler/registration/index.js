'use strict';

const router = require('express').Router();
const signUp = require('./service').auth;
const httpUtil = require('../../utils/HttpUtil');
const jwt = require('../../utils/JwtUtil');


//register
router.post('/signup',async (req,res)=>{
    try {
        let userReq = req.body;
        //validations goes hear
        //check for user existence
      
        let _data = await signUp.userexisting(userReq);
        if(_data.length > 0){
            res.json(httpUtil.getBadRequest('user exists kindly login',null));
        }else if(_data.length === 0){
            await signUp.saveUser(userReq);
            res.json(httpUtil.getSuccess('An sms has been sent to the given number',{contactNum : userReq.contactNum}));
        }
    } catch (error) {
        res.json(httpUtil.getBadRequest(error.toString(),null))
    }
});

router.post('/login',async (req,res)=>{
    try {
        let reqBody = req.body;
        let data = await signUp.login(reqBody);
        if(data.length === 1){
            //check for isconfirmed
            if(data[0].password === reqBody.password){
                let payload = {
                    token:jwt.signJWT(data[0]),
                    area:data[0].area,
                    catererId:data[0].catererId
                }
                res.json(httpUtil.getSuccess('logged in successfully',payload));
            }else{
                res.json(httpUtil.getBadRequest('Password is incorrect',null))
            }
        }else{
            res.json(httpUtil.getBadRequest('something went wrong',null))
        }
    } catch (error) {
        res.json(httpUtil.getBadRequest(error.toString(),null))
    }
})

router.post('/otpcheck',async (req,res)=>{
    try {
        signUp.checkOtp(req.body)
        .then(resolved => {
            res.json(httpUtil.getSuccess('Otp Verified',resolved))
        }).catch(rej => {
            res.json(httpUtil.getBadRequest(rej.toString(),null))
        })
    } catch (error) {
        res.json(httpUtil.getBadRequest(error.toString(),null))
    }
})

module.exports=router;