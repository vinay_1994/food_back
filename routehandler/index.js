const router = require('express').Router();

const registrationHandler = require('./registration')


router.use('/auth',registrationHandler)


module.exports=router
