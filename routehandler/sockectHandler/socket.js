const io = require('socket.io')()


let caterer = []


io.on('connection',async (socket) => {
    try {
        
        //caterer joining to room
        socket.on('JOIN',(catererData)=>{
            socket.join(catererData.area,(err)=>{
                catererData['id'] = socket.id;
                caterer.push(catererData);
                if(!err){
                    socket.emit('JOIN_CONFIRMATION',{status:true})
                }else{
                    socket.emit('JOIN_CONFIRMATION',{status:false})
                }
            })
            
        })

        //user search request
        socket.on('SEARCH_REQUEST',(searchData)=>{
            console.log(searchData)
            searchData['socketId'] = socket.id
            io.in(searchData.area).emit('BROADCAST_SEARCH_REQUEST',{searchData});
            //socket.emit('SEARCH_ACKNOWLEDGEMENT',{status:true})
        })

        socket.on('CATERER_ACCEPTED',(acceptedData)=>{
            io.to(`${acceptedData.socketId}`).emit('CATERER_ACCEPTED_FOR_USER',acceptedData);

        })
    } catch (error) {
        console.log(error)
    }
})





module.exports = io